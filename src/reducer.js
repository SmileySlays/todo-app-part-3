import todosList from "./todos.json";
import {
  CLEAR_COMPLETED_TODOS,
  DELETE_TODO,
  ADD_TODO,
  TOGGLE_TODO
} from "./actions.js";

const initialState = {
  todos: todosList
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO: {
      let newTodo = {
        userId: 1,
        id: Math.random(),
        title: action.payload,
        completed: false
      };

      const todos = [...state.todos, newTodo];

      return {
        ...state,
        todos
      };
    }

    case DELETE_TODO:
      return {
        ...state,
        todos: state.todos.filter(todo => todo.id !== action.payload)
      };

    case TOGGLE_TODO:
      return {
        ...state,
        todos: state.todos.map(todo =>
          todo.id === action.payload
            ? { ...todo, completed: !todo.completed }
            : todo
        )
      };

    case CLEAR_COMPLETED_TODOS:
      const newTodos = state.todos.filter(todo => todo.completed !== true);
      return {
        ...state,
        todos: newTodos
      };

    default:
      return state;
  }
};

export default reducer;
