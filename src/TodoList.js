import React, { Component } from "react";
import TodoItem from "./TodoItem";
import { toggleTodo, deleteTodo } from "./actions.js";
import { connect } from "react-redux";

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map(todo => (
            <TodoItem
              handleDeleteTodo={() => this.props.deleteTodo(todo.id)}
              key={todo.id}
              title={todo.title}
              completed={todo.completed}
              handleToggleTodo={() => this.props.toggleTodo(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

const mapDispatchToProps = {
  toggleTodo,
  deleteTodo
};

export default connect(
  null,
  mapDispatchToProps
)(TodoList);
