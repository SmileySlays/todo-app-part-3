import React, { Component } from "react";
import "./index.css";
//import todosList from "./todos.json";
import TodoList from "./TodoList";
import { Route, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { addTodo, clearCompletedTodos } from "./actions";

class App extends Component {
  state = {
    value: ""
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  handleCreateTodo = event => {
    if (event.key === "Enter") {
      this.props.addTodo(event.target.value);
      this.setState({ value: "" });
    }
  };

  // handleDeleteTodo = (event, todoIdToDelete) => {
  //   this.props.deleteTodo(todoIdToDelete);
  // };

  // handleCompleted = id => {
  //   this.props.toggleTodo(id);
  // };

  // handleAllCompleted = () => {
  //   this.props.clearCompletedTodos();
  // };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            value={this.state.value}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <Route
          exact
          path="/"
          render={() => (
            <TodoList
              // handleDeleteTodo={this.handleDeleteTodo}
              todos={this.props.todos}
              // handleCompleted={this.handleCompleted.bind(this)}
              // handleAllCompleted={this.handleAllCompleted}
            />
          )}
        />
        <Route
          exact
          path="/completed"
          render={() => (
            <TodoList
              // handleDeleteTodo={this.handleDeleteTodo}
              todos={this.props.todos.filter(todo => todo.completed === true)}
              // handleCompleted={this.handleCompleted.bind(this)}
              // handleAllCompleted={this.handleAllCompleted}
            />
          )}
        />
        <Route
          exact
          path="/active"
          render={() => (
            <TodoList
              // handleDeleteTodo={this.handleDeleteTodo}
              todos={this.props.todos.filter(todo => todo.completed !== true)}
              // handleCompleted={this.handleCompleted.bind(this)}
              // handleAllCompleted={this.handleAllCompleted}
            />
          )}
        />
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>
              {(() => {
                console.log(this.props);
                let incomplete = this.props.todos.filter(
                  todo => todo.completed !== true
                );
                return incomplete.length;
              })()}
            </strong>{" "}
            item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">
                All
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/active" activeClassName="selected">
                Active
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/completed" activeClassName="selected">
                Completed
              </NavLink>
            </li>
          </ul>
          <button
            onClick={this.props.clearCompletedTodos}
            className="clear-completed"
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

// class TodoList extends Component {
//   render() {
//     return (
//       <section className="main">
//         <ul className="todo-list">
//           {this.props.todos.map((todo, index) => (
//             <TodoItem
//               id={index}
//               handleDeleteTodo={event =>
//                 this.props.handleDeleteTodo(event, todo.id)
//               }
//               title={todo.title}
//               completed={todo.completed}
//               handleCompleted={this.props.handleCompleted}
//               handleAllCompleted={() => this.props.handleAllCompleted}
//             />
//           ))}
//         </ul>
//       </section>
//     );
//   }
// }

const mapStateToProps = state => {
  return {
    todos: state.todos
  };
};

const mapDispatchToProps = {
  addTodo,
  clearCompletedTodos
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
